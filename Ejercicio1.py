# Capitulo 4
# Ejercicio 6
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"

# Ejercicio 6: Reescribe el programa de cálculo del salario, con tarifa-y-media para las horas extras, y crea una función llamada calculo_salario que reciba dos parámetros (horas y tarifa)

def calculo_salario (salario , tarifa):
    if salario <= 40:
        total = salario * tarifa
        print("La cantidad que ustede resive es de ", total, " dolares")
    elif salario >= 41:
        salario -= 40
        total = 40 * tarifa
        incremento = 1.5 * (salario * tarifa)
        total += incremento
        print("La cantidad que ustede resive es de ", total, " dolares")
    else:
        print("La cantidad que Usted ingreso es incorrecta")

try:
    saldo = float(input("Ingrese el numero de horas trabajadas: "))
    tarifa = float(input("Engrese el monto por hora: "))
    calculo_salario(saldo,tarifa)

except:
    print("La cantidad que Usted ingreso es incorrecta")
