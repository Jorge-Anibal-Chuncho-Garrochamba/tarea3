# Capitulo 4
# Ejercicio 6
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"
# Ejercicio 7: Reescribe el programa de calificaciones del capítulo ante-rior usando una función llamada calcula_calificacion, que reciba una puntuación como parámetro y devuelva una calificación como cadena.

def calcula_calificacion(a):
    if a >= 0 and a <=  1.0:
        if a >= 0.9:
            print("Sobresaliente")
        elif a >= 0.8:
            print("Notable")
        elif a >= 0.7:
            print("Bien")
        elif a <= 0.6:
            print("Insuficiente")
    else:
        print("Fuera del rango")

try:
    puntuacion = float(input("Introduzca puntuacion: "))
    calcula_calificacion(puntuacion)
except:
    print("Puntuacion incorrecta")
